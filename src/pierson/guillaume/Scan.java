package pierson.guillaume;

import javax.swing.*;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class Scan {

    private static String localName;
    private static String localIPAdress;
    private static String localIPAdressPublic;


    public static boolean testIPAddress(int[] IPAddress){
        Process process;
        boolean isConnected = false;

        if (IPAddress.length != 4){
            JOptionPane.showMessageDialog(null,"Adresse IP non valide !", "Problème d'adresse IP", JOptionPane.ERROR_MESSAGE);
        } else {
            String IPAddressStr = String.valueOf(IPAddress[0])  + "." + String.valueOf(IPAddress[1]) + "." + String.valueOf(IPAddress[2]) + "." + String.valueOf(IPAddress[3]);
            try {
                process = java.lang.Runtime.getRuntime().exec("ping -w 2 -n 2 " + IPAddressStr);
                int returnVal = process.waitFor();
                isConnected = (returnVal == 0);
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }
        return isConnected;
    }

    public static String addressName(int[] IPAddress) throws UnknownHostException {

        byte[] ipB = {(byte) IPAddress[0], (byte) IPAddress[1], (byte) IPAddress[2], (byte) IPAddress[3]};
        InetAddress pingAddress = InetAddress.getByAddress(ipB);

        return pingAddress.getHostName();
    }

    public static boolean portIsOpen(int[] IPAddress, int port, int timeout){
        String IPAddressStr = String.valueOf(IPAddress[0])  + "." + String.valueOf(IPAddress[1]) + "." + String.valueOf(IPAddress[2]) + "." + String.valueOf(IPAddress[3]);

        try {
            Socket socket = new Socket();
            socket.connect(new InetSocketAddress(IPAddressStr, port), timeout);
            socket.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }



    public static void localhost(){
        try{
            InetAddress localAddress = InetAddress.getLocalHost();
            //nom de machine
            localName = localAddress.getHostName();
            //System.out.println("Nom de la machine = " + localName);
            //adresse ip sur le réseau
            localIPAdress = localAddress.getHostAddress();
            //System.out.println("Adresse IP locale = " + localIPAdress);

            localIPAdressPublic = localAddress.toString();
            //System.out.println("Adresse IP public = " + localIPAdressPublic);

        } catch (UnknownHostException uhe) {
            uhe.printStackTrace();
        }
    }

    public static String getLocalName() {
        return localName;
    }

    public static String getLocalIPAdress() {
        return localIPAdress;
    }

    public String getLocalIPAdressPublic() {
        return localIPAdressPublic;
    }
}
