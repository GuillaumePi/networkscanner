package pierson.guillaume.entites;

import java.util.ArrayList;

public class Device {
    private String adresseIP;
    private String nom;
    private boolean ssh;
    private boolean ftp;
    private boolean http;
    private boolean https;

    private static ArrayList<Device> listDevice = new ArrayList<>();

    public Device(String adresseIP, String nom, boolean ssh, boolean ftp, boolean http, boolean https) {
        this.setAdresseIP(adresseIP);
        this.setNom(nom);
        this.setSsh(ssh);
        this.setFtp(ftp);
        this.setHttp(http);
        this.setHttps(https);

        listDevice.add(this);
    }

    public String getAdresseIP() {
        return adresseIP;
    }

    public void setAdresseIP(String adresseIP) {
        this.adresseIP = adresseIP;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public boolean isSsh() {
        return ssh;
    }

    public void setSsh(boolean ssh) {
        this.ssh = ssh;
    }

    public boolean isFtp() {
        return ftp;
    }

    public void setFtp(boolean ftp) {
        this.ftp = ftp;
    }

    public boolean isHttp() {
        return http;
    }

    public void setHttp(boolean http) {
        this.http = http;
    }

    public boolean isHttps() {
        return https;
    }

    public void setHttps(boolean https) {
        this.https = https;
    }

    public static ArrayList<Device> getListDevice() {
        return listDevice;
    }

    public static void setListDevice(ArrayList<Device> listDevice) {
        Device.listDevice = listDevice;
    }
}
