package pierson.guillaume;

import pierson.guillaume.visuel.Fenetre;

import javax.swing.*;


public class Application {

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch(Exception e){ }
        new Application();


    }

    public Application() {
        Fenetre frame = new Fenetre();
        frame.setVisible(true);
    }

}
