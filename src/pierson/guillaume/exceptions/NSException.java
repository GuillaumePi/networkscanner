package pierson.guillaume.exceptions;

import java.net.UnknownHostException;

public class NSException extends Exception {
    public NSException(String message) {
        super(message);
    }

}
