package pierson.guillaume.visuel;

import pierson.guillaume.Scan;
import pierson.guillaume.entites.Device;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;

public class Fenetre extends JFrame {

    private final int SSH_PORT = 22;
    private final int FTP_PORT = 21;
    private final int HTTP_PORT = 80;
    private final int HTTPS_PORT = 443;
    private final int DEFAULT_TIMEOUT = 100;

    private JPanel panLocal = new JPanel(new FlowLayout(FlowLayout.LEFT, 50, 5));
    private JPanel panLocalName = new JPanel();
    private JPanel panLocalAddress = new JPanel();
    private JPanel panPing = new JPanel(new FlowLayout(FlowLayout.LEFT, 50, 5));
    private JPanel panPingAddress = new JPanel();
    private JPanel panNetwork = new JPanel();

    private JLabel lblLocalName = new JLabel("Nom : ");
    private JLabel lblLocalNameInput = new JLabel();
    private JLabel lblLocalAddress = new JLabel("Adresse IP : ");
    private JLabel lblLocalAddressInput = new JLabel();

    private JLabel lblDot1 = new JLabel(" . ");
    private JLabel lblDot2 = new JLabel(" . ");
    private JLabel lblDot3 = new JLabel(" . ");
    private JLabel lblFleche = new JLabel(" -> ");

    private JProgressBar progressBar = new JProgressBar(SwingConstants.HORIZONTAL);

    private JTextField txtIP1 = new JTextField(5);
    private JTextField txtIP2 = new JTextField(5);
    private JTextField txtIP3 = new JTextField(5);
    private JTextField txtIP4 = new JTextField(5);
    private JTextField txtIP5 = new JTextField(5);

    private JButton btnPing = new JButton("Ping");

    private final String[] ENTETE_NETWORK = {"IP", "Nom", "SSH", "FTP", "HTTP", "HTTPS" };

    private DefaultTableModel tableModelNetwork = new DefaultTableModel(ENTETE_NETWORK, 0)
    {
        @Override
        public boolean isCellEditable(int row, int column)
        {
            return false;
        }
    };

    private JTable tblNetwork = new JTable();

    private String name;

    public Fenetre() {

        this.setSize(800, 600);
        this.setTitle("Network scanner");

        ImageIcon icone = new ImageIcon("imgLifeGame.png");
        this.setIconImage(icone.getImage());

        this.setResizable(false);

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getWidth() / 2, dim.height / 2 - this.getHeight() / 2);

        init();
    }

    private void init(){
        Border borderLocal = BorderFactory.createTitledBorder("Localhost : ");
        panLocal.setBorder(borderLocal);

        Scan.localhost();

        lblLocalNameInput.setText(Scan.getLocalName());
        lblLocalAddressInput.setText(Scan.getLocalIPAdress());

        panLocalName.setLayout(new BoxLayout(panLocalName, BoxLayout.X_AXIS));
        panLocalName.add(lblLocalName);
        panLocalName.add(lblLocalNameInput);
        panLocal.add(panLocalName);

        panLocalAddress.setLayout(new BoxLayout(panLocalAddress, BoxLayout.X_AXIS));
        panLocalAddress.add(lblLocalAddress);
        panLocalAddress.add(lblLocalAddressInput);
        panLocal.add(panLocalAddress);

        Border borderPingAdress = BorderFactory.createTitledBorder("Adresse IP : ");
        panPingAddress.setBorder(borderPingAdress);

        txtIP1.setText("192");
        txtIP2.setText("168");
        txtIP3.setText("1");
        txtIP4.setText("1");
        txtIP5.setText("254");

        progressBar.setValue(0);
        progressBar.setStringPainted(true);
        progressBar.setVisible(true);

        btnPing.addActionListener(e -> {
            int[] IPAddress = {Integer.parseInt(txtIP1.getText()), Integer.parseInt(txtIP2.getText()), Integer.parseInt(txtIP3.getText()), 0 };
            double IPMax = Integer.parseInt(txtIP5.getText());

            SwingWorker<Void, Void> worker = new SwingWorker<>() {
                @Override
                protected Void doInBackground() {


                    for (int i = Integer.parseInt(txtIP4.getText()); i <= IPMax; i++) {
                        IPAddress[3] = i;
                        double status = (double) (i / IPMax);
                        progressBar.setValue((int)(status * 100));
                        if (Scan.testIPAddress(IPAddress)) {
                            boolean sshPort = Scan.portIsOpen(IPAddress, SSH_PORT, DEFAULT_TIMEOUT);
                            boolean ftpPort = Scan.portIsOpen(IPAddress, FTP_PORT, DEFAULT_TIMEOUT);
                            boolean httpPort = Scan.portIsOpen(IPAddress, HTTP_PORT, DEFAULT_TIMEOUT);
                            boolean httpsPort = Scan.portIsOpen(IPAddress, HTTPS_PORT, DEFAULT_TIMEOUT);
                            try {
                                name = Scan.addressName(IPAddress);
                            } catch (UnknownHostException unknownHostException) {
                                unknownHostException.printStackTrace();
                            }
                            Device device = new Device((String.valueOf(IPAddress[0]) + "." + String.valueOf(IPAddress[1]) + "." + String.valueOf(IPAddress[2]) + "." + String.valueOf(IPAddress[3])), name, sshPort, ftpPort, httpPort, httpsPort);

                            updateTable();

                        }
                    }
                    return null;
                }
            };
            worker.execute();
        });

        panPingAddress.setLayout(new BoxLayout(panPingAddress, BoxLayout.X_AXIS));
        panPingAddress.add(txtIP1);
        panPingAddress.add(lblDot1);
        panPingAddress.add(txtIP2);
        panPingAddress.add(lblDot2);
        panPingAddress.add(txtIP3);
        panPingAddress.add(lblDot3);
        panPingAddress.add(txtIP4);
        panPingAddress.add(lblFleche);
        panPingAddress.add(txtIP5);

        Border borderPing = BorderFactory.createTitledBorder("Ping : ");
        panPing.setBorder(borderPing);

        panPing.add(panPingAddress);
        panPing.add(btnPing);
        panPing.add(progressBar);

        Border borderNetwork = BorderFactory.createTitledBorder("Network : ");
        panNetwork.setBorder(borderNetwork);

        for (Device i : Device.getListDevice()) {
            tableModelNetwork.addRow(new Object[]{i.getAdresseIP(), i.getNom(), i.isSsh(), i.isFtp(), i.isHttp(), i.isHttps()});
        }

        tblNetwork.setModel(tableModelNetwork);
        tblNetwork.setRowSelectionAllowed(true);
        tblNetwork.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tblNetwork.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tblNetwork.setSize(500,300);
        tblNetwork.getColumnModel().getColumn(0).setPreferredWidth(80);
        tblNetwork.getColumnModel().getColumn(1).setPreferredWidth(120);
        tblNetwork.getColumnModel().getColumn(2).setPreferredWidth(60);
        tblNetwork.getColumnModel().getColumn(3).setPreferredWidth(60);
        tblNetwork.getColumnModel().getColumn(4).setPreferredWidth(60);
        tblNetwork.getColumnModel().getColumn(5).setPreferredWidth(60);

        panNetwork.add(new JScrollPane(tblNetwork, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER), BorderLayout.CENTER);

        Container contentPane = this.getContentPane();
        contentPane.add(panLocal, BorderLayout.PAGE_END);
        contentPane.add(panPing, BorderLayout.PAGE_START);
        contentPane.add(panNetwork, BorderLayout.CENTER);


    }

    private void updateTable(){
        DefaultTableModel tableModelEmpty = (DefaultTableModel) tblNetwork.getModel();
        tableModelEmpty.setRowCount(0);

        for (Device i : Device.getListDevice()) {
            tableModelNetwork.addRow(new Object[]{i.getAdresseIP(), i.getNom(), i.isSsh(), i.isFtp(), i.isHttp(), i.isHttps()});
        }

        tblNetwork.setModel(tableModelNetwork);
    }
}
